import React, { Component } from "react";
import io from "socket.io-client";

class App extends Component {
  state = {
    isConnected: false,
    id: null,
    users: [],
    old_messagem: [],
    messagem: []
  };
  socket = null;

  //  message = {
  //   text: null,
  //   id: null,
  //   name: George
  // }

  componentWillMount() {
    this.socket = io("https://codi-server2.herokuapp.com");

    this.socket.on("connect", () => {
      this.setState({ isConnected: true });
    });
    this.socket.on("pong!", (additionalStuff) => {
      console.log("server answered!", additionalStuff);
    });

    this.socket.on("youare", (answer) => {
      this.setState({ id: answer.id });
    });

    this.socket.on("peeps", (users) => {
      this.setState({ users: users });
    });

    this.socket.on("newconnection", (users) => {
      this.setState({ users: users.push() });
    });

    this.socket.on("newdisconnection", (users) => {
      this.setState({ users: users.pop() });
    });

    this.socket.on("next", (message_from_server) =>
      console.log(message_from_server)
    );

    this.socket.on("disconnect", () => {
      this.setState({ isConnected: false });
    });



    /** this will be useful way, way later **/
    // this.socket.on("room", (old_messages) => {
    //   console.log(old_messages[0])
    //   this.setState({old_messagem:old_messages})
    // });

    this.socket.on("room_message", (message) => {
      
      console.log(message)

      
      this.setState({messagem:[...this.state.messagem, message]})
      console.log("hiiiii console")
    });

 
  }

 

  componentWillUnmount() {
    this.socket.close();
    this.socket = null;
  }

  render() {
  
    return (
      <div className="App">
        {/* <div>
          status: {this.state.isConnected ? "connected" : "disconnected"}
        </div>
        <div>id: {this.state.id}</div>
        <button onClick={() => this.socket.emit("ping!")}>ping</button> */}
        <button onClick={() => this.socket.emit("whoami")}>Who am I?</button>
        {/* <p>Number of users: {this.state.users.length}</p>
        <div>Users IDs:</div>
        <div>
          <p>{this.state.users.join("\n | ")}</p>
        </div>

       <button onClick={() => this.socket.emit("give me next")}>Message</button>
       <br></br> */}
        {/* <button onClick={() => this.socket.emit("message")}>Send</button> */}

        <input id="userId" type="text"></input>{" "}
        <button
          onClick={() => {
            this.socket.emit("message", {
              text: document.getElementById("userId").value,
              id: this.state.id,
              name: "George",
            });
          }}
        >
          Send
        </button>
        {/* <button
          onClick={() => {
           console.log(this.state.messagem)
          }}
        >
          Display
        </button> */}
          {/* <p id = "oldmessage">
          {this.state.old_messagem.map((m,k) => (
            <ul key = {k}>
              <li>
              <h3>Name:  {m.name}</h3>
              </li>
              <li>
                <p>Message: {m.text}</p>
              </li>
            </ul>
            
          ))}
          </p> */}
        
            <p id = "textDisplay">
          {this.state.messagem.map((m,k) => (
            <ul key = {k}>
              <li>
             <bold> Name:  </bold>{m.name}
              </li>
              <li>
                <p>Message: {m.text}</p>
                </li>
                <li>
                <p>Date: {m.date}</p>
              </li>
              
            </ul>
            
          ))}
          </p>
        <div>
          
        </div>
      </div>
    );
  }
}

export default App;
